"use strict";

function rectanglesUnion(recs) {
    let sumField = 0
    function field(rec) {return (rec[2]-rec[0]) * (rec[3]-rec[1])}
    function subRec(rec1, rec2) {return [
        Math.max(rec1[0], rec2[0]),
        Math.max(rec1[1], rec2[1]),
        Math.min(rec1[2], rec2[2]),
        Math.min(rec1[3], rec2[3])
    ]}
    for(let i=0; i<recs.length; i++) {
        //dodaje pole prostokąta
        sumField += field(recs[i])
        let recsIn = []
        //szukam cześci wspólnych z poprzednimi prostokątami
        for(let j=0; j<=i; j++) {
            if(i===j)
                continue
            let recIn = subRec(recs[i], recs[j])
            if(recIn[0] < recIn[2] && recIn[1] < recIn[3]) {
                //dodaje część wspólną jako wewnętrzny prostkąt
                recsIn.push(recIn)
            }
        }
        //odejmuj pola części wspólnych
        for(let j=0; j<recsIn.length; j++) {
            sumField -= field(recsIn[j])
            //szukam części wspólnych wewnętrznych prostokątów aby je dodać gdy odejmowałem je wielokrotnie
            for(let k=0; k<=j; k++) {
                if(j===k)
                    continue
                let commonRecIn = subRec(recsIn[j], recsIn[k])
                if(commonRecIn[0] < commonRecIn[2] && commonRecIn[1] < commonRecIn[3]) {
                    sumField += field(commonRecIn)
                }
            }
        }
    }
    return sumField;
}

var assert = require('assert');
if (!global.is_checking) {
    console.log('Example:')
    console.log(rectanglesUnion([
        [6,3,8,10],
        [4,8,11,10],
        [16,8,19,11],
        [6,8,8,12]
    ]))

    // These "asserts" are used for self-checking and not for an auto-testing
    assert.equal(rectanglesUnion([
        [6, 3, 8, 10],
        [4, 8, 11, 10],
        [16, 8, 19, 11]
    ]), 33)
    assert.equal(rectanglesUnion([
        [16, 8, 19, 11]
    ]), 9)
    assert.equal(rectanglesUnion([
        [16, 8, 19, 11],
        [16, 8, 19, 11]
    ]), 9)
    assert.equal(rectanglesUnion([
        [16, 8, 16, 8]
    ]), 0)
    assert.equal(rectanglesUnion([
        
    ]), 0)
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}