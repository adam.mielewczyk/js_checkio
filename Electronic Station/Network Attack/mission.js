"use strict";

function capture(data) {
	let counter = -1
	let toInfect = []
	do{
		for (let i in data) {
			if (data[i][i] === 0) {
				if (toInfect.indexOf(i) !== -1)
					toInfect.splice(toInfect.indexOf(i), 1)
				for (let j in data)
					if (data[i][j] === 1 && toInfect.indexOf(j) === -1 && data[j][j]!==0)
						toInfect.push(j)
			}
		}
		for (let toInfectIndex of toInfect) {
				data[toInfectIndex][toInfectIndex]--
		}
		counter++
	}while (toInfect.length)
    return counter;
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(capture([[0, 1, 0, 1, 0, 1],
	                      [1, 8, 1, 0, 0, 0],
	                      [0, 1, 2, 0, 0, 1],
	                      [1, 0, 0, 1, 1, 0],
	                      [0, 0, 0, 1, 3, 1],
	                      [1, 0, 1, 0, 1, 2]]), 8, "Base example");
    assert.equal(capture([[0, 1, 0, 1, 0, 1],
	                      [1, 1, 1, 0, 0, 0],
	                      [0, 1, 2, 0, 0, 1],
	                      [1, 0, 0, 1, 1, 0],
	                      [0, 0, 0, 1, 3, 1],
	                      [1, 0, 1, 0, 1, 2]]), 4, "Low security");
    assert.equal(capture([[0, 1, 1],
                          [1, 9, 1],
                          [1, 1, 9]]), 9, "Small");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}