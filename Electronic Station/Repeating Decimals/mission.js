"use strict";

function convert(numerator, denominator) {
    let remainders = [numerator % denominator]
    let ans = Math.floor(numerator / denominator).toString() + "."
    let bracket = ""
    while(true) {
        let remainder = remainders[remainders.length-1] * 10 % denominator
        ans += Math.floor(remainders[remainders.length-1] * 10 / denominator).toString()
        if(remainder === 0) {
            ans = (numerator / denominator).toString()
            if(ans.indexOf(".") === -1)
                return ans + "."
            else
                return ans
        }
        else if(remainders.indexOf(remainder) !== -1) {
            for(let i=0; i<(remainders.length - remainders.indexOf(remainder)); i++) {
                bracket += Math.floor(remainders[remainders.indexOf(remainder) + i] * 10 / denominator).toString()
            }
            break
        }
        remainders.push(remainder)
    }
    return ans.substr(0, ans.length-bracket.length) + "(" + bracket + ")";
}

var assert = require('assert');
if (!global.is_checking) {
    // These "asserts" are used for self-checking and not for an auto-testing
    assert.equal(convert(290, 12), "24.1(6)")
    assert.equal(convert(11, 7), "1.(571428)")

    assert.equal(convert(1, 3), "0.(3)")
    assert.equal(convert(5, 3), "1.(6)")
    assert.equal(convert(3, 8), "0.375")
    assert.equal(convert(7, 11), "0.(63)")


    assert.equal(convert(0, 117), "0.")
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}