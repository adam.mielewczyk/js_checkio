"use strict";

function brokenClock(startingTime, wrongTime, errorDescription){
    startingTime = startingTime.split(":")
    wrongTime = wrongTime.split(":")
    errorDescription = errorDescription.split(" ")
    startingTime = startingTime[0]*3600 + startingTime[1]*60 + parseInt(startingTime[2])
    wrongTime = wrongTime[0]*3600 + wrongTime[1]*60 + parseInt(wrongTime[2])
    function multiplierFromString(str) {
        if(str.indexOf("hour") !== -1)              return 3600
        else if(str.indexOf("minute") !== -1)       return 60
        else                                                    return 1
    }
    errorDescription[0] = parseInt(errorDescription[0]) * multiplierFromString(errorDescription[1])
    errorDescription[3] = parseInt(errorDescription[3]) * multiplierFromString(errorDescription[4])
    let ansInSec = Math.floor(Math.abs((startingTime-wrongTime)*errorDescription[3]/(errorDescription[0]+errorDescription[3]))+startingTime)
    let ans=[,,]
    ans[0] = Math.floor(ansInSec/3600)
    ansInSec =ansInSec%3600
    ans[1] = Math.floor(ansInSec/60)
    ansInSec = ansInSec%60
    ans[2] = Math.floor(ansInSec)
    return ("0" + ans[0]).slice(-2) + ":" + ("0" + ans[1]).slice(-2) + ":" + ("0" + ans[2]).slice(-2)
}

var assert = require('assert');

if (!global.is_checking) {
    console.log(parseInt(("00" + 10).slice(-3)))
    assert.equal(brokenClock('00:00:00', '00:00:15', '+5 seconds at 10 seconds'), '00:00:10', "First example");
    assert.equal(brokenClock('06:10:00', '06:10:15', '-5 seconds at 10 seconds'), '06:10:30', "Second example");
    assert.equal(brokenClock('13:00:00', '14:01:00', '+1 second at 1 minute'), '14:00:00', "Third example");
    assert.equal(brokenClock('01:05:05', '04:05:05', '-1 hour at 2 hours'), '07:05:05', "Fourth example");
    assert.equal(brokenClock('00:00:00', '00:00:30', '+2 seconds at 6 seconds'), '00:00:22', "Fifth example");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
