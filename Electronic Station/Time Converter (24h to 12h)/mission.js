"use strict";

function timeConverter(dayTime) {
    let time_splited = dayTime.split(":")
    if(time_splited[0] !== "12" && time_splited[0] !== "00")
        return parseInt(time_splited[0]) % 12
               + ":" + time_splited[1]
               + (parseInt(time_splited[0])<12 ? " a.m." : " p.m.")
    else
        return "12:" + time_splited[1] + (parseInt(time_splited[0])<12 ? " a.m." : " p.m.")
}

var assert = require('assert');
if (!global.is_checking) {
    console.log('Example:')
    console.log(timeConverter('12:30'))

    // These "asserts" are used for self-checking and not for an auto-testing
    assert.equal(timeConverter('12:30'), '12:30 p.m.')
    assert.equal(timeConverter('09:00'), '9:00 a.m.')
    assert.equal(timeConverter('23:15'), '11:15 p.m.')
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}