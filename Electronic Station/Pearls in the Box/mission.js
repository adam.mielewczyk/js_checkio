"use strict";
function boxProbability(marbles, step) {
    let probOfWhiteAtEnd = 0;
    function count(str, char) {
        let counter = 0;
        for(let c of str) {
            if(c === char)
                counter++
        }
        return counter
    }
    let pTable = [[{ w: count(marbles,"w"), b: count(marbles,"b"), p: 1}]]
    for(let i=1; i<=step; i++){
        pTable.push([]);
        for(let j=0; j<pTable[i-1].length; j++){
            if(pTable[i-1][j].w > 0) {
                pTable[i].push({w: pTable[i-1][j].w - 1, b: pTable[i-1][j].b + 1, p:0})
                pTable[i][pTable[i].length-1].p = pTable[i-1][j].p * pTable[i-1][j].w / (pTable[i-1][j].b +pTable[i-1][j].w)
                if(step===i){
                    probOfWhiteAtEnd += pTable[i][pTable[i].length-1].p;
                }
            }
            if(pTable[i-1][j].b > 0) {
                pTable[i].push({w: pTable[i-1][j].w + 1, b: pTable[i-1][j].b - 1, p:0})
                pTable[i][pTable[i].length-1].p = pTable[i-1][j].p * pTable[i-1][j].b / (pTable[i-1][j].b +pTable[i-1][j].w)
            }
        }
    }
    return Math.round(probOfWhiteAtEnd*100)/100
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(boxProbability('bbw', 3), 0.48, "First");
    assert.equal(boxProbability('wwb', 3), 0.52, "Second");
    assert.equal(boxProbability('www', 3), 0.56, "Third");
    assert.equal(boxProbability('bbbb', 1), 0, "Fifth");
    assert.equal(boxProbability('wwbb', 4), 0.5, "Sixth");
    assert.equal(boxProbability('bwbwbwb', 5), 0.48, "Seventh");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}