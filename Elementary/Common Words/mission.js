"use strict";

function commonWords(first, second) {
    first = first.split(",")
    second = second.split(",")
    let ans = []
    for(let wordFromFirst of first)
        for(let wordFromSecond of second)
            if(wordFromFirst === wordFromSecond)
                ans.push(wordFromSecond)
    return ans.sort().join(",");
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(commonWords("hello,world", "hello,earth"), "hello", "Hello");
    assert.equal(commonWords("one,two,three", "four,five,six"), "", "Too different");
    assert.equal(commonWords("one,two,three", "four,five,one,two,six,three"), "one,three,two", "1 2 3");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
