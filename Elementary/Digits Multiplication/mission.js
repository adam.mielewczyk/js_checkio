"use strict";

function digitsMultip(data) {
    data = data.toString();
    let ans = 1;
    for(let char of data)
    {
        ans *= char!=="0" ? parseInt(char) : 1;
    }
    return ans;
}

var assert = require('assert');

if (!global.is_checking) {
    console.log('Example:')
    console.log(digitsMultip(123405))
    
    assert.equal(digitsMultip(123405), 120, "1st");
    assert.equal(digitsMultip(999), 729, "2nd");
    assert.equal(digitsMultip(1000), 1, "3rd");
    assert.equal(digitsMultip(1111), 1, "4th");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
