"use strict";

function countingTiles(r){
    let R = Math.ceil(Math.sqrt(2 * r**2))
    let solidCounter = 0
    let partialCounter = 0
    for(let i=0; i<=R; i++) {
        for(let j=0; j<=R; j++){
            let tempR = Math.sqrt(i**2 + j**2)
            if(tempR <= r && i>0 && j>0)
                solidCounter++
            else if(tempR < r)
                partialCounter++
        }
    }
    return [solidCounter*4, partialCounter*4]
}

var assert = require('assert');

if (!global.is_checking) {
    assert.deepEqual(countingTiles(2), [4, 12], "N=2");
    assert.deepEqual(countingTiles(3), [16, 20], "N=3");
    assert.deepEqual(countingTiles(2.1), [4, 20], "N=2.1");
    assert.deepEqual(countingTiles(2.5), [12, 20], "N=2.5");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
