"use strict";

function createIntervals(data) {
    let ans = []
    data.sort(function (a, b) {
        return a - b
    });
    let start = data[0]
    let end = start - 1
    for (let integer of data)
        if (end + 1 === integer)
            end = integer
        else {
            ans.push([start, end])
            start = integer
            end = integer
        }
    if(start)
        ans.push([start, end])
    return ans
}

var assert = require('assert');

if (!global.is_checking) {
    assert.deepEqual(createIntervals([1, 2, 3, 4, 5, 7, 8, 12]), [[1, 5], [7, 8], [12, 12]], "First")
    assert.deepEqual(createIntervals([1, 2, 3, 6, 7, 8, 4, 5]), [[1, 8]], "Second")
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}