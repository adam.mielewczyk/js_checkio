"use strict";

function brackets(expression){
    let possibleBracket = [["{", "(", "["], ["}", ")", "]"]]
    let bracketsFromExpression = []
    for(let char of expression) {
        for(let i in possibleBracket[0]) {
            if(char === possibleBracket[0][i])
                bracketsFromExpression.push(char)
            else if(char === possibleBracket[1][i]){
                if(bracketsFromExpression[bracketsFromExpression.length-1] === possibleBracket[0][i])
                    bracketsFromExpression.pop()
                else
                    return false
            }
        }
    }
    return !bracketsFromExpression.length;
}

var assert = require('assert');

if (!global.is_checking) {
    console.log(brackets("(({[(((1)-2)+3)-3]/3}-3)"))
    assert.equal(brackets("((5+3)*2+1)"), true, "Simple");
    assert.equal(brackets("{[(3+1)+2]+}"), true, "Different types");
    assert.equal(brackets("(3+{1-1)}"), false, ") is alone inside {}");
    assert.equal(brackets("[1+1]+(2*2)-{3/3}"), true, "Different operators");
    assert.equal(brackets("(({[(((1)-2)+3)-3]/3}-3)"), false, "One is redundant");
    assert.equal(brackets("2+3"), true, "No brackets, no problem");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}