"use strict";

function reverseRoman(roman) {
    let ans = 0
    let romanTab = [[900,"CM"], [1000,"M"], [400,"CD"], [500,"D"], [90,"XC"], [100,"C"], [40,"XL"], [50,"L"], [9,"IX"], [10,"X"], [4,"IV"], [5,"V"], [1,"I"]]
    for(let romVal of romanTab) {
        while(true) {
            if(roman.indexOf(romVal[1]) !== -1) {
                ans += romVal[0]
                roman = roman.replace(romVal[1], "")
            }
            else
                break
        }
    }
    return ans;
}

var assert = require('assert');

if (!global.is_checking) {
    reverseRoman("MMMCMXCIX")
    assert.equal(reverseRoman('VI'), 6, "First")
    assert.equal(reverseRoman('LXXVI'), 76, "Second")
    assert.equal(reverseRoman('CDXCIX'), 499, "Third")
    assert.equal(reverseRoman('MMMDCCCLXXXVIII'), 3888, "Forth")
    console.log("Coding complete, Cesar? Click 'Check' to review your tests and earn cool rewards!");
}