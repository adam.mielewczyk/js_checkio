"use strict";

function fastTrain(numbers) {
    ///////wydajność algorytmu pewnie można by zwiększyć używając systemu trójkkowego w części odpowiadającej za spowalnianie zamiast metody prób i błedów//////
    //dodaje przystanek jako odcinek o maksymalnej prędkości 0
    numbers.push([1,0])
    let accels = []
    //stwierdzam że pociąg cały czas przyśiesza
    for(let array of numbers)
        for(let i=0; i<array[0]; i++)
            accels.push(1)
    //sprawdź czy limity prędkości nie przekroczone i zwolnij pociąg jeśli tak
    let speed = 0;
    let distance = 0
    let time = 0
    for(let i=0; i<accels.length; i++) {
        speed += accels[i]
        // gdy prędkość zero i czas nie był jeszcze zapisany to w tym momencie dojechał
        if(speed === 0 && time === 0)
            return i;
        // stopniowo dodaje dystans przejechany i sprawdzam ograniczenia
        // aby unkinąć systuacji "przeskoczenia" ograniczenia
        for(let j=1; j<=speed; j++) {
            distance++
            //obliczam maksymalną prędkość w danym miejscu
            let maxSpeed;
            let tempSumDistannce = 0;
            for(let array of numbers) {
                tempSumDistannce += array[0];
                if(tempSumDistannce >= distance) {
                    maxSpeed = array[1];
                    break;
                }
            }
            if (speed > maxSpeed) {
                // ostatnie przyśpieszenia zmieniam na utrzymanie prędkości/zwolnienia
                // robie to "bitowo" - kiedy nie mogę zwolnić ostatniego to zwalniam kolejnego ale poprzedniego powiększam
                for(let k=i; k>=0; k--) {
                    if(accels[k] > -1) {
                        accels[k]--;
                        break;
                    }
                    else
                        accels[k]=1;
                }
                //i lecimy od nowa ze sprawdzaniem bo mogłem nie wyhamować dostatecznie
                i = -1;
                speed = 0;
                distance = 0;
                break;
            }
        }
    }
}

var assert = require('assert');

if (!global.is_checking) {
    console.log('Example:');
    console.log(fastTrain([[4, 3]]));

    assert.equal(fastTrain([[4, 3]]), 3);
    assert.equal(fastTrain([[9, 10]]), 5);
    assert.equal(fastTrain([[5, 5], [4, 2]]), 6);
    console.log('"Run" is good. How is "Check"?');
}


