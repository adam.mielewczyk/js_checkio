"use strict";

function weakPoint(matrix){
    let minRow = 0;
    let minRowSum = Number.MAX_VALUE;
    let minCol = 0;
    let minColSum = Number.MAX_VALUE;
    for(let i in matrix)
    {
        let colSum = 0;
        let rowSum = 0;
        for(let j in matrix) {
            colSum += matrix[j][i];
            rowSum += matrix[i][j];
        }
        if(colSum<minColSum)
        {
            minColSum = colSum;
            minCol = i;
        }
        if(rowSum<minRowSum)
        {
            minRowSum = rowSum;
            minRow = i;
        }
    }
    return [parseInt(minRow), parseInt(minCol)]
}

var assert = require('assert');

if (!global.is_checking) {

    assert.deepEqual(weakPoint([[7, 2, 7, 2, 8],
                                [2, 9, 4, 1, 7],
                                [3, 8, 6, 2, 4],
                                [2, 5, 2, 9, 1],
                                [6, 6, 5, 4, 5]]
                                ), [3, 3], "Example");
    assert.deepEqual(weakPoint([[7, 2, 4, 2, 8],
                                [2, 8, 1, 1, 7],
                                [3, 8, 6, 2, 4],
                                [2, 5, 2, 9, 1],
                                [6, 6, 5, 4, 5]]
                                ), [1, 2], "Two weak point");

    assert.deepEqual(weakPoint([[1, 1, 1],
                                [1, 1, 1],
                                [1, 1, 1]]
                                ), [0, 0], "Top left");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}