"use strict";

function superRoot(number) {
    function f(x) {return x ** x - number}
    function df(x) {return (Math.log(x) + 1) * x ** x}
    let x = Math.log(number)
    let lastX = 0
    while(Math.abs(lastX-x)>0.000001) {
        lastX = x
        x = lastX - f(lastX)/df(lastX)
    }
    return x;
}

var assert = require('assert');
if (!global.is_checking) {
    console.log('Example:')
    console.log(superRoot(10000000000))

    // These "asserts" using only for self-checking and not necessary for auto-testing
    function checkResult(func, number) {
        var result = func(number);
        var p = result ** result;
        if (number - 0.001 < p && p < number + 0.001) {
            return true
        } else {
            return false
               }
        };

    var firstTest = checkResult(superRoot, 4);
    var secondTest = checkResult(superRoot, 9);
    var thirdTest = checkResult(superRoot, 81);

    assert.equal(firstTest, true);
    assert.equal(secondTest, true);
    assert.equal(thirdTest, true);
    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}