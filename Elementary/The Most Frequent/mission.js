"use strict";

function mostFrequent(data) {
    // returns the most frequenly occuring string in the given array
    let freqString = ""
    let freqStringCount = 0
    for(let stringToCount of data)
    {
        let stringCounter = 0
        for(let stringInData of data)
        {
            if(stringInData === stringToCount)
                stringCounter++
        }
        if(stringCounter > freqStringCount)
        {
            freqString = stringToCount
            freqStringCount = stringCounter
        }
    }
    // your code here
    return freqString;
}

var assert = require('assert');

if (!global.is_checking) {
    console.log('Example:')
    console.log(mostFrequent([
        'a', 'b', 'c', 
        'a', 'b',
        'a'
    ]))
    
    assert.equal(mostFrequent([
        'a', 'b', 'c', 
        'a', 'b',
        'a'
    ]), 'a')
    assert.equal(mostFrequent(['a', 'a', 'bi', 'bi', 'bi']), 'bi')
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
