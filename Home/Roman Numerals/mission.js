"use strict";

function romanNumerals(number) {
    let ans = "";
    let roman = {1000:"M", 900:"CM", 500:"D", 400:"CD", 100:"C", 90:"XC", 50:"L", 40:"XL", 10:"X", 9:"IX", 5:"V", 4:"IV", 1:"I"}
    for(let key of Object.keys(roman).reverse())
    {
        key = parseInt(key);
        let temp = roman[key];
        ans += roman[key].repeat(Math.floor(number/key));
        number %= key;
    }
    return ans;
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(romanNumerals(6), 'VI', "First");
    assert.equal(romanNumerals(76), 'LXXVI', "Second");
    assert.equal(romanNumerals(499), 'CDXCIX', "Third");
    assert.equal(romanNumerals(3888), 'MMMDCCCLXXXVIII', "Forth");
    console.log("Done! Go Check!");
}
