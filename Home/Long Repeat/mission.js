"use strict";

function longRepeat(line) {
    // length the longest substring that consists of the same char
    let maxCounter = 0;
    let lastChar = "";
    let counter = 0;
    for(let char of line)
    {
        if(lastChar === char)
        {
            counter++;
        }
        else
        {
            counter = 1;
            lastChar = char;
        }
        if(maxCounter < counter)
            maxCounter = counter;
    }

    // your code here
    return maxCounter;
}

var assert = require('assert');

if (!global.is_checking) {
    assert.equal(longRepeat('sdsffffse'), 4, "First")
    assert.equal(longRepeat('ddvvrwwwrggg'), 3, "Second")
    console.log('"Run" is good. How is "Check"?');
}