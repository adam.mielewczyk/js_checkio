"use strict";

function mostWanted(data) {
    data = data.toLowerCase();
    let freqLetter = "";
    let freqOfLetter = 0;
    for(let i = "a".charCodeAt(0); i<="z".charCodeAt(0); i++)
    {
        let counter = 0;
        let j = data.indexOf(String.fromCharCode(i));
        while(j!== -1)
        {
            counter++;
            j = data.indexOf(String.fromCharCode(i), j + 1);
        }
        if(counter>freqOfLetter)
        {
            freqLetter = String.fromCharCode(i);
            freqOfLetter = counter;
        }
    }
    return freqLetter;
}

var assert = require('assert');

if (!global.is_checking) {
    console.log(mostWanted("Z"))
    assert.equal(mostWanted("Hello World!"), "l", "1st example");
    assert.equal(mostWanted("How do you do?"), "o", "2nd example");
    assert.equal(mostWanted("One"), "e", "3rd example");
    assert.equal(mostWanted("Oops!"), "o", "4th example");
    assert.equal(mostWanted("AAaooo!!!!"), "a", "Letters");
    console.log("Coding complete? Click 'Check' to review your tests and earn cool rewards!");
}
