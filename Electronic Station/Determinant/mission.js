"use strict";

function determinant(data) {
    //minory do obliczenia w postaci [liczba przez którą trzeba pomnożyć wynik, minor]
    let toCalc = [[1,data]]
    //pętla tworząca toCalc
    for(let i=0; i<data.length-1; i++) {
        let nextToCalc = []
        //pętla po minorach które już są
        for(let j=0; j<toCalc.length; j++) {
            //pętla po minorach kótre mam stworzyć
            for(let k=0; k<toCalc[0][1].length; k++) {
                let minor = []
                //petla tworząca minor
                for(let l=1; l<toCalc[0][1].length; l++)
                    minor.push(toCalc[j][1][l].filter(function(currentValue, index, arr) {return index!==k}))
                //dodanie minora razem z liczą do pomnożenia wyniku
                nextToCalc.push([toCalc[j][0] * toCalc[j][1][0][k] * ((-1) ** (2+k)), minor])
            }
        }
        toCalc = nextToCalc
    }
    let ans = []
    //pomnożenie minorów jedno elementowych
    for(let minor1x1 of toCalc) {
        ans.push(minor1x1[0]*minor1x1[1][0][0])
    }
    return ans.reduce(function(a, b) {return a + b})
}

var assert = require('assert');
if (!global.is_checking) {
    // These "asserts" are used for self-checking and not for an auto-testing
    assert.equal(determinant([[4, 3],
                                    [6, 3]]), -6)
    assert.equal(determinant([[1, 3, 2],
                                    [1, 1, 4],
                                    [2, 2, 1]]), 14)



    console.log("Coding complete? Click 'Check' to earn cool rewards!");
}
